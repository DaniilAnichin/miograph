#!/usr/bin/python
# -*- coding: utf-8 -*- #
import sys
import matplotlib.pyplot as plt
import numpy as np


def get_rrs(rheogram):
    strikes = unpopular_indexes(rheogram)
    distances = np.array(strikes) - np.array([0] + strikes[:-1])

    merging = 0
    dist = []
    for i, x in enumerate(distances[1:-1]):
        if x < 15:
            merging += x
        else:
            if len(dist) > 0:
                dist[-1] += merging / 2.0
            dist.append(x + merging / 2.0)
            merging = 0

    print dist
    # plt.plot(rheogram)
    # plt.ylabel('Rheogram')

    # plt.show()
    # return popular_values(dist)
    return dist


def unpopular_indexes(array):
    bins = 25
    hist, bin_edges = np.histogram(array, bins=bins)
    moda = bin_edges[np.argmax(hist)]
    middle = (bin_edges[0] + bin_edges[-1]) / 2
    if moda > middle:
        lower = 2 * moda - bin_edges[-1]
        upper = bin_edges[-1]
    else:
        lower = bin_edges[0]
        upper = 2 * moda - bin_edges[0]

    # plt.plot(array)
    plt.plot([x if lower <= x <= upper else 0 for x in array])
    # plt.ylabel('Rheogram')

    plt.show()
    return [i for i, x in enumerate(array) if lower <= x <= upper]


def popular_values(array):
    bins = 15
    radius = 3
    hist, bin_edges = np.histogram(array, bins=bins)
    moda_index = np.argmax(hist)
    lower = bin_edges[max(moda_index - radius, 0)]
    upper = bin_edges[min(moda_index + radius, bin_edges.shape[0])]

    return [x for i, x in enumerate(array) if lower <= x <= upper]

def main():
    args = sys.argv[1:]
    if not args:
        print "Run like `python ./pulser.py /path/to/csv`"
        return -1
    else:
        data_path = args[0]

    reo = np.genfromtxt(
        data_path, delimiter=';', names=True, dtype=None
    )['REO']

    all_dist = []

    batches = 20
    half_size = reo.shape[0] / (batches + 1)
    for i in range(batches):
        all_dist += get_rrs(reo[i * half_size:(i + 2) * half_size])

    pulses = map(lambda x: round(12000. / x, 1), all_dist)

    common = popular_values(pulses)

    print 'Results:'
    print common

    print 'Deviation:'
    print round(np.std(common), 1)

if __name__ == '__main__':
    main()
