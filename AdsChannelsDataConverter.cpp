#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <stdlib.h>

using namespace std;

#define ADS_MEASURED_DATA_SIZE_IN_BYTES	  (27)
#define ADS_VREF                          (2.4)
#define ADS_OUR_REF_SCALER                (100)


bool ConvertFile(string& strFileName, int nScaler, bool bFirstColumnNumbers);


int main(int argc, char** argv)
{
/*
*/
    //system("pause");

    cout << "Parameters (not obligatory):" << endl;
    cout << "1st: file name for conversion ('AdsRawData.txt')." << endl;
    cout << "2nd: scaler for Vref (100 if parameter absent)." << endl;
    cout << "3rd: 'N' Numbers of samples column and Caption are absent (present by default)." << endl;
    cout << endl;

    //system("Pause");

    //Set default parameters
    string strAdsDataFileName("AdsRawData.txt");
    int nScaler = 0;
    bool bFirstColumnNumbers = true;

    // Read file name
    if (argc >= 2)  {
        strAdsDataFileName = argv[1];
    }

    // Read scaler if available otherwise - default value is zero
    if (argc >= 3)  {
        string strAdsDataScaler(argv[2]);
        nScaler = atoi(strAdsDataScaler.data());

        if (nScaler < 0)
            nScaler = -nScaler;

    }

    if (argc >= 4)  {
        if (toupper(argv[3][0]) == 'N')
            bFirstColumnNumbers = false;
    }

    if (argc >= 5)  {
      cout << "Invalid parameters number" << endl;

    }
    //string strAdsDataFileName("real-my.txt");
    //int nScaler = 0;


    cout << "ADS channels parser with present Status register. Conversion..." << endl;

    if ( ConvertFile(strAdsDataFileName, nScaler, bFirstColumnNumbers) )
            cout << "Conversion completed." << endl;
    else
            cout << "Conversion not done." << endl;


    //system("Pause");

    return 0;
}




int FindOffsetOfValidDataBlock(ifstream& InFile)
{
    // Returns -1 if cannot find 0xC0 marker
    int nOffset = -1;


    //char Block[2 * ADS_MEASURED_DATA_SIZE_IN_BYTES];
    unsigned char Block[2 * ADS_MEASURED_DATA_SIZE_IN_BYTES];
    InFile.read( (char*)Block, 2 * ADS_MEASURED_DATA_SIZE_IN_BYTES);

    for (int i=0; i<ADS_MEASURED_DATA_SIZE_IN_BYTES; i++)  {
        if (Block[i] == 0xC0)  {
            if (Block[i + ADS_MEASURED_DATA_SIZE_IN_BYTES] == 0xC0)  {
                nOffset = i;
                break;
            }
        }
    }

    return nOffset;
}




bool ConvertFile(string& strFileName, int nScaler, bool bFirstColumnNumbers)
{
    string strOutFileName_IntOriginal = strFileName + "-IntOriginal.txt";
 
    ifstream InFile (strFileName.data(), std::ifstream::binary);
    if (!InFile)  {
        cout << "Input file open error. Perhaps file '" << strFileName.data() << "' does not exist." << endl;
        return false;
    }

    // Find start offset of the valid data block
    int nInputDataOffset = FindOffsetOfValidDataBlock(InFile);
    if (nInputDataOffset < 0)  {
        cout << "Input file (" << strFileName.data() << ") does not consist of valid data (0xC0 marker was not found)." << endl;
        return false;
    }


    ofstream OutFile_IntOriginal (strOutFileName_IntOriginal.data(), std::ofstream::trunc);
 
    if (!OutFile_IntOriginal)
        return false;

    // Calculate size of data in input file started from nInputDataOffset
    InFile.seekg(nInputDataOffset, InFile.end);
    int nInFileSize = InFile.tellg();

    // Seek file pointer to nInputDataOffset
    InFile.seekg(nInputDataOffset, InFile.beg);

    // Calculate valid blocks of measured data that had been captured to the input file
    int nValidChannelsDataBlocks = nInFileSize / ADS_MEASURED_DATA_SIZE_IN_BYTES;

    // Add Caption
    if (bFirstColumnNumbers)  {
        OutFile_IntOriginal << "Sample No; Ch1; Ch2; Ch3; Ch4; Ch5; Ch6; Ch7; Ch8;" << endl;
    }

    // Read and convert all blocks - block by block
    for (int i=0; i < nValidChannelsDataBlocks; i++)  {
        char Block[ADS_MEASURED_DATA_SIZE_IN_BYTES];

        // Write first column as consecutive number of measured sample
        if (bFirstColumnNumbers)  {
            OutFile_IntOriginal << i + 1 << ";";
        }

        // Read next data block (1 Status reg + 8 channels data)
        InFile.read(Block, ADS_MEASURED_DATA_SIZE_IN_BYTES);

        // Set pointer to first channel data
        unsigned char* pChannelData = (unsigned char*)Block +3;

        // Extract and convert data for each from 8 channels
        for (int j=0; j<8; j++)  {
            int nChannelData = (int)(pChannelData[2]);
            nChannelData = (((int)(pChannelData[1])) << 8) | nChannelData;
            nChannelData = (((int)(pChannelData[0])) << 16) | nChannelData;

            if (pChannelData[0] & 0x80)
                nChannelData |= 0xFF000000;

            OutFile_IntOriginal << nChannelData << ";";

            double dVref = ADS_VREF;
            double dOurRef = nScaler ? dVref * double(nScaler) : dVref * double(ADS_OUR_REF_SCALER);
            double dChannelData = nChannelData;
            double dChanMaxValue = (double(int(0x7FFFFF)));

            double dValForVref = (dChannelData / dChanMaxValue) * dVref;
            double dValForOurRef  = (dChannelData / dChanMaxValue) * dOurRef;

            pChannelData += 3;
        }

        OutFile_IntOriginal << endl;
    }
    return true;
}
