#!/usr/bin/python
# -*- coding: utf-8 -*- #
import sys
import matplotlib.pyplot as plt
import numpy as np
import pywt
colors = [
    'blue',
    'yellowgreen',
    'cyan',
    'magenta',
    'black',
    'red',
    'orange',
    'darkgreen',
]
plot_params = dict(
    left=0.08,
    right=0.99,
    bottom=0.03,
    top=0.99,
    wspace=0,
    hspace=0,
)


def main():
    args = sys.argv[1:]
    if not args:
        print "Run like `python ./extrraact.py /path/to/csv`"
        return -1
    else:
        data_path = args[0]

    # Read the data from txt-files, strip unused, transform matrix
    mio = np.genfromtxt(
        data_path, delimiter=';', skip_header=1, 
        usecols=range(1, 9), dtype=int
    ).transpose()

    first = None
    figure = plt.figure()
    for i in range(len(mio)):
        # Move graph down
        mio[i] = mio[i] - mio[i][0]
        if not first:
            ax = figure.add_subplot(8, 1, i+1)
            first = ax
        else:
            ax = figure.add_subplot(8, 1, i+1, sharex=first)
        ax.set_xlim(0, mio.shape[-1])
        ax.plot(mio[i], color=colors[i])
        ax.grid(True)

    plt.subplots_adjust(**plot_params)
    plt.show()


if __name__ == '__main__':
    main()
